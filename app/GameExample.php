<?php

class GameExample extends \Lobby\GameAbstract{
    public function onCommand($input, \Lobby\Player $player) {
        
        if (is_object($input) && isset($input->action)){
            switch ($input->action){
                case 'exampleAction':
                    $df = new Lobby\DataFrame('exampleResponse', $input);
                    foreach ($this->getPlayers() as $player){
                        $player->send($df);
                    }
                    
                    break;
            }
        }
        
    }
}