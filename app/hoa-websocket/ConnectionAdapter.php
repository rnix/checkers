<?php

class DummySocket {

    public function isConnected() {
        return true;
    }

}

class ConnectionAdapter {

    protected $_source;
    protected $_id;
    protected $_node;

    public function __construct($source) {
        $this->_source = $source;
        $this->_node = $this->_source->getConnection()->getCurrentNode();
        $this->_id = $this->_source->getConnection()->getCurrentNode()->getId();
    }

    public function getId() {
        return $this->_node->getId();
    }

    public function send($data) {
        $this->_source->send($data, $this->_node);
    }

    public function getSocket(){
        return new DummySocket();
    }

}

