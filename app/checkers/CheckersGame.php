<?php

include_once 'UnitPosition.php';

class CheckersGame extends \Lobby\GameAbstract{
    
    public $whitePlayer;
    public $blackPlayer;
    
    public $state;
    
    public $positions;
    public $possibleMoves;
    public $kings;
    
    public $haveToAttack = false;
    public $winningSide;
    
    public $moves;
    
    public function onCommand($input, \Lobby\Player $player) {
        
        if (is_object($input) && isset($input->action)){
            switch ($input->action){
                case 'onStart':
                    if ($this->state && ($this->state !== "sideSelect" && $this->state !== "endMatch")){
                        return;
                    }
                    $this->state = "sideSelect";
                    if ($this->_getPlayerIndex($player) === 0){
                        $df = new Lobby\DataFrame("selectSide");
                        $player->send($df);
                        
                        $dfOther = new Lobby\DataFrame("waitForSide");
                        foreach ($this->getPlayers() as $p){
                            if ($p->getId() !== $player->getId()){
                                $p->send($dfOther);
                            }
                        }
                    }
                    $this->_setStartPositions();
                    $this->winningSide = null;
                    break;
                case 'playAs':
                    if ($this->state !== "sideSelect"){
                        return;
                    }
                    $side = $input->data;
                    $secondPlayer = $this->_getPlayerByIndex(1);
                    if ($this->_getPlayerIndex($player) === 0){
                        if ($side == "white"){
                            $this->whitePlayer = $player;
                            $this->blackPlayer = $secondPlayer;
                        } else {
                            $this->whitePlayer = $secondPlayer;
                            $this->blackPlayer = $player;
                        }
                    }
                    
                    foreach ($this->getPlayers() as $p){
                        if ($p === $this->whitePlayer){
                            $df = new Lobby\DataFrame("selectedSide", "white");
                        } else if ($p === $this->blackPlayer){
                            $df = new Lobby\DataFrame("selectedSide", "black");
                        } else {
                            $df = new Lobby\DataFrame("selectedSide", "none");
                        }
                        $p->send($df);
                    }
                    
                    $this->_changeState("allocate");
                    $this->_changeState("whiteTurn", false);
                    $this->_calcPossibleMoves();
                    $this->_changeState("whiteTurn", true);
                    
                    break;
                case "move":
                    $this->_moveCommand($input, $player);
                    break;
                case "getState":
                    $player->send($this->_getStateDataFrame());
                    break;
            }
        }
        
    }
    
    protected function _setStartPositions(){
        $this->positions = new stdClass();
        $this->positions->white = new stdClass();
        $this->positions->black = new stdClass();
        
        $this->positions->white->id0 = new UnitPosition(0, 5);
        $this->positions->white->id1 = new UnitPosition(2, 5);
        $this->positions->white->id2 = new UnitPosition(4, 5);
        $this->positions->white->id3 = new UnitPosition(6, 5);
        
        $this->positions->white->id4 = new UnitPosition(1, 6);
        $this->positions->white->id5 = new UnitPosition(3, 6);
        $this->positions->white->id6 = new UnitPosition(5, 6);
        $this->positions->white->id7 = new UnitPosition(7, 6);
        
        $this->positions->white->id8 = new UnitPosition(0, 7);
        $this->positions->white->id9 = new UnitPosition(2, 7);
        $this->positions->white->id10 = new UnitPosition(4, 7);
        $this->positions->white->id11 = new UnitPosition(6, 7);
        
        $this->positions->black->id0 = new UnitPosition(1, 0);
        $this->positions->black->id1 = new UnitPosition(3, 0);
        $this->positions->black->id2 = new UnitPosition(5, 0);
        $this->positions->black->id3 = new UnitPosition(7, 0);
        
        $this->positions->black->id4 = new UnitPosition(0, 1);
        $this->positions->black->id5 = new UnitPosition(2, 1);
        $this->positions->black->id6 = new UnitPosition(4, 1);
        $this->positions->black->id7 = new UnitPosition(6, 1);
        
        $this->positions->black->id8 = new UnitPosition(1, 2);
        $this->positions->black->id9 = new UnitPosition(3, 2);
        $this->positions->black->id10 = new UnitPosition(5, 2);
        $this->positions->black->id11 = new UnitPosition(7, 2);
        
        $this->kings = new stdClass();
        $this->kings->white = array();
        $this->kings->black = array();
        
        $this->moves = 0;
    }
    
    protected function _calcPossibleMoves(){
        $this->possibleMoves = new stdClass();
        $this->possibleMoves->white = new stdClass();
        $this->possibleMoves->black = new stdClass();
        
        if ($this->state == 'whiteTurn'){
            $side = 'white';
            $opSide = "black";
        } else {
            $side = 'black';
            $opSide = "white";
        }
        
        $movesById = array();
        $attackMovesById = array();
        foreach ($this->positions->$side as $id => $unitPos){
            $possibilities = array();
            $attackPossibilities = array();
            if ($side == "white") {
                $possibilities[] = new UnitPosition($unitPos->x - 1, $unitPos->y - 1);
                $possibilities[] = new UnitPosition($unitPos->x + 1, $unitPos->y - 1);
                
                $attackPossibilities[] = new UnitPosition($unitPos->x - 1, $unitPos->y + 1);
                $attackPossibilities[] = new UnitPosition($unitPos->x + 1, $unitPos->y + 1);
            } else {
                $possibilities[] = new UnitPosition($unitPos->x - 1, $unitPos->y + 1);
                $possibilities[] = new UnitPosition($unitPos->x + 1, $unitPos->y + 1);
                
                $attackPossibilities[] = new UnitPosition($unitPos->x - 1, $unitPos->y - 1);
                $attackPossibilities[] = new UnitPosition($unitPos->x + 1, $unitPos->y - 1);
            }
            
            
            $moves = array();
            $attackMoves = array();
            foreach ($possibilities as $posibPos) {
                if ($posibPos->isValid()) {

                    if (!$this->isPosEngaged($posibPos)) {
                        $moves[] = $posibPos;
                    } else if ($this->canAttack($side, $id, $posibPos)) {
                        $attackMoves[] = $unitPos->getJumpLandingPos($posibPos);
                    }
                }
            }
            
            foreach ($attackPossibilities as $posibPos){
                if ($posibPos->isValid() && $this->canAttack($side, $id, $posibPos)){
                    $attackMoves[] = $unitPos->getJumpLandingPos($posibPos);
                }
            }
            
            
            
            if ($this->isKing($side, $id)){
                $moves = array();
                $attackMoves = array();
                $kingPossibleMoves = $this->getKingPossibleMovesFromPos($unitPos, $side);
                if ($kingPossibleMoves["attackMoves"]){
                    $subAttackFound = false;
                    foreach ($kingPossibleMoves["attackMoves"] as $subPos){
                        $subKingMoves = $this->getKingPossibleMovesFromPos($subPos, $side);
                        if ($subKingMoves["attackMoves"] && array_diff($subKingMoves["attackMoves"], $unitPos->getAllDiagonalWithPos($subPos)) ){
                            $attackMoves[] = $subPos; 
                            $subAttackFound = true;
                        }
                    }
                    if (!$subAttackFound){
                        $attackMoves = $kingPossibleMoves["attackMoves"];
                    }
                } else {
                    $moves = $kingPossibleMoves["moves"];
                }
            }
            
            
            
            if ($attackMoves){
                $attackMovesById[$id] = $attackMoves;
            } else {
                $movesById[$id] = $moves;
            }
            
        }
        
        if ($attackMovesById){
            /* only attack is possible */
            $movesById = $attackMovesById;
            $this->haveToAttack = true;
        } else {
            $this->haveToAttack = false;
        }
        foreach ($movesById as $id=>$moves){
            $this->possibleMoves->$side->$id = $moves;
        }
    }
    
    public function getKingPossibleMovesFromPos(UnitPosition $unitPos, $side) {
        if ($side == "white"){
            $opSide = "black";
        } else {
            $opSide = "white";
        }
        $moves = array();
        $attackMoves = array();
        $possibilities = $unitPos->getAllDiagonals();
        foreach ($possibilities as $direction => $diagonalPos) {
            $i = 0;
            $canMove = true;
            $foundEngagedByEnemy = false;
            while ($canMove && isset($diagonalPos[$i])) {
                if (!$this->isPosEngaged($diagonalPos[$i])) {

                    if ($foundEngagedByEnemy) {
                        $attackMoves[] = $diagonalPos[$i];
                    } else {
                        $moves[] = $diagonalPos[$i];
                    }
                } else {

                    if ($foundEngagedByEnemy) {
                        $canMove = false;
                    } else if ($this->getSideOnPos($diagonalPos[$i]) === $opSide) {
                        $foundEngagedByEnemy = true;
                    } else {
                        $canMove = false;
                    }
                }
                $i++;
            }
        }
        return array("moves" => $moves, "attackMoves" => $attackMoves);
    }
    
    public function isPosEngaged(UnitPosition $up){
        foreach ($this->positions as $sidePositions){
            foreach ($sidePositions as $id=>$pos){
                if ($pos && $up->x === $pos->x && $up->y === $pos->y){
                    return true;
                }
            }
        }
        return false;
    }
    


    protected function _getPlayerByIndex($index){
        $i = 0;
        foreach ($this->getPlayers() as $p){
            if ($i === $index){
                return $p;
            }
            $i++;
        }
        return false;
    }
    
    protected function _getPlayerIndex($player){
        $i = -1;
        foreach ($this->getPlayers() as $p){
            $i++;
            if ($p->getId() === $player->getId()){
                return $i;
            }
        }
        return false;
    }
    
    protected function _changeState($state, $send = true){
        $this->state = $state;
        if ($send){
            $this->_sendState();
        }
    }
    
    protected function _getStateDataFrame() {
        $data = new stdClass();
        $data->state = $this->state;
        if ($this->state == 'whiteTurn' || $this->state == 'blackTurn' || $this->state == 'allocate') {
            $data->possibleMoves = $this->possibleMoves;
            $data->positions = $this->positions;
            $data->kings = $this->kings;
        } else if ($this->state == 'endMatch') {
            $data->winningSide = $this->winningSide;
        }
        
        if ($this->state == 'allocate'){
            if ($this->whitePlayer){
                $data->whiteSidePlayerId = $this->whitePlayer->getId();
            }
            if ($this->blackPlayer){
                $data->blackSidePlayerId = $this->blackPlayer->getId();
            }
        }
        $df = new Lobby\DataFrame('state', $data);
        return $df;
    }


    protected function _sendState(){
        $df = $this->_getStateDataFrame();
        foreach ($this->getPlayers() as $p){
            $p->send($df);
        }
    }
    
    protected function isPossibleToMove($side, $unitId, UnitPosition $newPos) {
        $possibleMoves = $this->possibleMoves->$side->$unitId;
        foreach ($possibleMoves as $posMove) {
            if ($posMove->x === $newPos->x && $posMove->y === $newPos->y) {
                return true;
            }
        }
        return false;
    }
    
    protected function _moveCommand($input, \Lobby\Player $player) {
        if (($this->state === "whiteTurn" && $this->whitePlayer === $player) || ($this->state === "blackTurn" && $this->blackPlayer === $player)) {
            
            if ($this->whitePlayer === $player){
                $side = "white";
                $opSide = "black";
            } else {
                $side = "black";
                $opSide = "white";
            }
            
            $newPos = new UnitPosition($input->data->newPos->x, $input->data->newPos->y);
            $unitId = $input->data->id;
            if ($this->isPossibleToMove($side, $unitId, $newPos)){
                $removed = $this->_removeAttackedUnit($side, $this->positions->$side->$unitId, $newPos);
                $this->positions->$side->$unitId = $newPos;
                if ($newPos->isKingLine($side)){
                    array_push($this->kings->$side, $unitId);
                }
                
                $this->_calcPossibleMoves();
                if ($removed && $this->haveToAttack && isset($this->possibleMoves->$side->$unitId)) {
                    //just update
                    $this->_changeState($side."Turn");
                } else {
                    //change turn
                    $this->_changeState($opSide . "Turn", false);
                    $this->_calcPossibleMoves();
                    
                    if ($this->isPossibleToContinue()) {
                        $this->_changeState($opSide . "Turn");
                    } else {
                        $this->_endMatch($side);
                    }
                    
                }
                $this->moves++;
            } else {
                $this->_changeState($side."Turn");
            }
        }
    }
    
    public function getSideOnPos(UnitPosition $pos){
        foreach ($this->positions as $side=>$sidePositions){
            foreach ($sidePositions as $id=>$iterPos){
                if ($iterPos && $iterPos->x === $pos->x && $iterPos->y === $pos->y){
                    return $side;
                }
            }
        }
    }
    
    public function _endMatch($winningSide){
        if ($this->state !== 'endMatch') {
            $this->winningSide = $winningSide;
            $this->_changeState("endMatch");
            if ($this->moves > 10) {
                if ($winningSide === "white") {
                    $winner = $this->whitePlayer;
                } else {
                    $winner = $this->blackPlayer;
                }
                if ($winner) {
                    $winner->getModel()->addWin(true);
                }
            }
        }
    }

    public function isPossibleToContinue(){
        $side = null;
        if ($this->state == "whiteTurn"){
            $side = "white";
        } else if ($this->state =="blackTurn"){
            $side = "black";
        }
        
        if ($side){
            foreach ($this->possibleMoves->$side as $moves){
                if ($moves){
                    return true;
                }
            }
            return false;
        }
    }
    
    public function isKing($side, $id){
        return ($this->kings && isset($this->kings->$side) && in_array($id, $this->kings->$side));
    }

    public function canAttack($side, $id, UnitPosition $posibPos){
        if ($side === "white"){
            $opSide = "black";
        } else {
            $opSide = "white";
        }
        $posSide = $this->getSideOnPos($posibPos);
        $pos = $this->positions->$side->$id;
        
        if ($posSide && $posSide==$opSide && $pos){
            $jumpLandingPos = $pos->getJumpLandingPos($posibPos);
            if ($jumpLandingPos && !$this->isPosEngaged($jumpLandingPos)){
                return true;
            }
        }
        return false;
    }
    
    protected function _removeAttackedUnit($mySide, UnitPosition $posStart, UnitPosition $posEnd){
        $transitional = $posStart->getAllTransitional($posEnd);
        if ($mySide == "white"){
            $opSide = "black";
        } else {
            $opSide = "white";
        }
        $removed = false;
        foreach ($this->positions->$opSide as $id=>$pos) {
            if ($pos && in_array($pos, $transitional)){
                $this->positions->$opSide->$id = null;
                unset($this->positions->$opSide->$id);
                $removed = true;
            }
        }
        return $removed;
        
    }
    
    public function removePlayer(\Lobby\Player $player){
        parent::removePlayer($player);
        if ($player === $this->whitePlayer){
            $this->_endMatch("black");
            $this->whitePlayer = null;
        } else if ($player === $this->blackPlayer){
            $this->_endMatch("white");
            $this->blackPlayer = null;
        }
    }
    
    public function onLaterJoinRoom($player){ 
        /* observers */
        $this->addPlayer($player);
        if ($this->whitePlayer && !$this->blackPlayer){
            $this->blackPlayer = $player;
            $side = "black";
        } else if (!$this->whitePlayer && $this->blackPlayer){
            $this->whitePlayer = $player;
            $side = "white";
        } else {
            $side = "none";
        }
        $df = new Lobby\DataFrame("selectedSide", $side);
        $player->send($df);
        /* force allocate*/
        if ($this->whitePlayer && $this->blackPlayer) {
            $data = new stdClass();
            $data->state = "allocate";
            $data->possibleMoves = $this->possibleMoves;
            $data->positions = $this->positions;
            $data->kings = $this->kings;

            $data->whiteSidePlayerId = $this->whitePlayer->getId();
            $data->blackSidePlayerId = $this->blackPlayer->getId();
            $df = new Lobby\DataFrame('state', $data);
            $player->send($df);
        }
        
    }
}