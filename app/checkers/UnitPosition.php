<?php

class UnitPosition {

    public $x;
    public $y;

    public function __construct($x, $y) {
        $this->x = $x;
        $this->y = $y;
    }

    public function isValid() {
        if ($this->x >= 0 && $this->x <= 7 && $this->y >= 0 && $this->y <= 7) {
            if (($this->y % 2 !== 0 && $this->x % 2 === 0) || ($this->y % 2 === 0 && $this->x % 2 !== 0)) {
                return true;
            }
        }
        return false;
    }

    public function getJumpLandingPos(UnitPosition $jumpPos) {
        $x = $jumpPos->x + ($jumpPos->x - $this->x);
        $y = $jumpPos->y + ($jumpPos->y - $this->y);
        $land = new UnitPosition($x, $y);
        if ($land->isValid()) {
            return $land;
        }
        return false;
    }

    /**
     * 
     * Returns cells between this and other
     * 
     * @param UnitPosition $endPos
     * @return \UnitPosition
     */
    public function getAllTransitional(UnitPosition $endPos) {
        $transitional = array();
        if ($endPos->x > $this->x){
            $ofX = 1;
        } else {
            $ofX = -1;
        }
        
        if ($endPos->y > $this->y){
            $ofY = 1;
        } else {
            $ofY = -1;
        }

        $y = $this->y;
        for ($x = $this->x + $ofX; $x * $ofX < $endPos->x * $ofX; $x = $x + $ofX) {
            $y = $y + $ofY;
            $pos = new UnitPosition($x, $y);
            
            if ($pos->isValid()){
                $transitional[] = $pos;
            }
        }
        return $transitional;
    }
    
    public function getAllDiagonalWithPos(UnitPosition $endPos) {
        $diagonal = array();
        /* y = k*x + b */
        if ( ($endPos->x - $this->x) * ($endPos->y - $this->y) > 0 ){
            $k = 1;
        } else {
            $k = -1;
        }
        $b = $this->y - $k * $this->x;
        
        for ($x=0; $x<=7; $x++){
            $y = $k * $x + $b;
            $pos = new UnitPosition($x, $y);
            if ($pos->isValid()){
                $diagonal[] = $pos;
            }
        }
        
        return $diagonal;
    }

    public function getAllDiagonals() {
        $cells = array(
            "tr" => array(),
            "br" => array(),
            "bl" => array(),
            "tl" => array(),
        );

        $y1 = $this->y;
        $y2 = $this->y;
        for ($x = $this->x + 1; $x <= 7; $x++) {
            $y1--;
            $y2++;
            $cell1 = new UnitPosition($x, $y1);
            if ($cell1->isValid()) {
                $cells["br"][] = $cell1;
            }
            $cell2 = new UnitPosition($x, $y2);
            if ($cell2->isValid()) {
                $cells["tr"][] = $cell2;
            }
        }

        $y1 = $this->y;
        $y2 = $this->y;
        for ($x = $this->x - 1; $x >= 0; $x--) {
            $y1--;
            $y2++;
            $cell1 = new UnitPosition($x, $y1);
            if ($cell1->isValid()) {
                $cells["bl"][] = $cell1;
            }
            $cell2 = new UnitPosition($x, $y2);
            if ($cell2->isValid()) {
                $cells["tl"][] = $cell2;
            }
        }

        return $cells;
    }
    
    public function isKingLine($side){
        if ($side === "white"){
            return ($this->y === 0);
        } else {
            return ($this->y === 7);
        }
    }
    
    public function __toString() {
        return "x={$this->x};y={$this->y}";
    }

}
