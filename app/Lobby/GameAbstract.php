<?php

namespace Lobby;

/**
 * @property \Lobby\Player[] $_players
 * @property \Lobby\Room $_room
 */
abstract class GameAbstract implements GameInterface {
    protected $_room;
    protected $_id;
    protected $_players;
    
    public function __construct(Room $room) {
        $this->_room = $room;
        
        $this->id = md5($room->getOwner()->getConnection()->getId() . $room->name . time());
        foreach ($room->members as $player){
            $this->addPlayer($player);
        }
    }
    
    /**
     * 
     * @return string
     */
    public function getId(){
        return $this->id;
    }
    
    public function addPlayer(Player $player){
        $this->_players[$player->uid] = $player;
    }
    
    public function removePlayer(Player $player){
        unset($this->_players[$player->uid]);
    }
    
    /**
     * 
     * @return \Lobby\Player[]
     */
    public function getPlayers(){
        return $this->_players;
    }
    
    public function getRoom(){
        return $this->_room;
    }
    
    public function onLaterJoinRoom($player){
        
    }
}