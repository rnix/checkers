<?php

namespace Lobby;

class Player {
    protected $connection;
    public $uid;
    protected $model;
    public $name;
    protected $_disconncted = false;
    
    public function __construct($connection, $uid) {
        $this->connection = $connection;
        $this->uid = $uid;
        
        $this->model = \User::model()->findByPk($uid);
        if ($this->model){
            $this->name = \CHtml::encode($this->model->name);
        }
    }
    
    public function getConnection(){
        return $this->connection;
    }
    
    public function getModel(){
        return $this->model;
    }
    
    /**
     * Sends command to client connection
     * 
     * @param \Lobby\DataFrame $df
     */
    public function send(DataFrame $df){
        $connection = $this->connection;
        if (!$this->_disconncted && $connection){
            $socket = $connection->getSocket();
            if ($socket && $socket->isConnected()) {
                try {
                    $connection->send($df);
                } catch (Exception $e) {
                    echo $e->getMessage() . "\n";
                }
            }
        }
    }
    
    public function getId(){
        return $this->uid;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function markAsDisconnected(){
        $this->_disconncted = true;
    }
}