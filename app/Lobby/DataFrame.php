<?php
namespace Lobby;

class DataFrame extends \stdClass{
    
    public $action;
    public $data;
    
    public function __construct($action, $data = null) {
        $this->action = $action;
        $this->data = $data;
    }
    
    public function __toString() {
        return json_encode($this);
    }
    
}