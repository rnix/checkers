<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require(__DIR__ . '/SplClassLoader.php');

$classLoader = new SplClassLoader('Wrench', __DIR__);
$classLoader->register();

$lobbyClassLoader = new SplClassLoader('Lobby', __DIR__);
$lobbyClassLoader->register();

require(__DIR__ . '/LobbyApplication.php');

//array(
//    'allowed_origins'            => array(
//        'lobby.lh'
//    ),
// Optional defaults:
//     'check_origin'               => true,
//     'connection_manager_class'   => 'Wrench\ConnectionManager',
//     'connection_manager_options' => array(
//         'timeout_select'           => 0,
//         'timeout_select_microsec'  => 200000,
//         'socket_master_class'      => 'Wrench\Socket\ServerSocket',
//         'socket_master_options'    => array(
//             'backlog'                => 50,
//             'ssl_cert_file'          => null,
//             'ssl_passphrase'         => null,
//             'ssl_allow_self_signed'  => false,
//             'timeout_accept'         => 5,
//             'timeout_socket'         => 5,
//         ),
//         'connection_class'         => 'Wrench\Connection',
//         'connection_options'       => array(
//             'socket_class'           => 'Wrench\Socket\ServerClientSocket',
//             'socket_options'         => array(),
//             'connection_id_secret'   => 'asu5gj656h64Da(0crt8pud%^WAYWW$u76dwb',
//             'connection_id_algo'     => 'sha512'
//         )
//     )
//)

$server = new \Wrench\Server('ws://0.0.0.0:' . Yii::app()->params['webSocketServerPort'], Yii::app()->params['webSocketOptions']);

require __dir__ . '/checkers/CheckersGame.php';
$gameClassName = "CheckersGame";

$server->registerApplication('lobby', new LobbyApplication($gameClassName));
$server->run();
