<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require(__DIR__ . '/SplClassLoader.php');

$lobbyClassLoader = new SplClassLoader('Lobby', __DIR__);
$lobbyClassLoader->register();

require(__DIR__ . '/LobbyApplication.php');
require (__DIR__ . '/checkers/CheckersGame.php');
require (__DIR__ . '/hoa-websocket/ConnectionAdapter.php');
require (__DIR__ . '/hoa-websocket/PayloadAdapter.php');
$gameClassName = "CheckersGame";
$lobbyApplication =  new LobbyApplication($gameClassName);

class LobbyApplicationContainer {

    static $app;

    public static function setApp($app) {
        self::$app = $app;
    }

    public static function getApp() {
        $app = self::$app;
        if ($app) {
            return $app;
        } else {
            throw new Exception("app is not defined");
        }
    }

}

LobbyApplicationContainer::setApp($lobbyApplication);


require __dir__.'/hoa-websocket/vendor/autoload.php';

$websocket = new Hoa\Websocket\Server(
    new Hoa\Socket\Server('tcp://0.0.0.0:' . Yii::app()->params['webSocketServerPort'])
);
$websocket->on('open', function ( Hoa\Core\Event\Bucket $bucket ) {
    echo 'new connection';
    $connection = new ConnectionAdapter($bucket->getSource());
    LobbyApplicationContainer::getApp()->onConnect($connection);
    echo ": " . $connection->getId(), "\n";
    return;
});
$websocket->on('message', function ( Hoa\Core\Event\Bucket $bucket ) {
    $data = $bucket->getData();
    $payload = new PayloadAdapter($data);
    $connection = new ConnectionAdapter($bucket->getSource());
    LobbyApplicationContainer::getApp()->onData($payload, $connection);
    return;
});
$websocket->on('close', function ( Hoa\Core\Event\Bucket $bucket ) {
    $connection = new ConnectionAdapter($bucket->getSource());
    LobbyApplicationContainer::getApp()->onDisconnect($connection);
    return;
});
$websocket->run();

