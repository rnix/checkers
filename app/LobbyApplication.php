<?php

/**
 * @property \Lobby\Room[] $rooms
 * @property \Lobby\Player[] $players
 */

/* class LobbyApplication extends Wrench\Application\Application { */
class LobbyApplication  {

    protected $clients = array();
    protected $rooms = array();
    protected $players = array();
    protected $games = array();
    
    protected $lastTimestamp = null;
    protected $gameClassName;
    protected $refreshDbInterval = 15; //min;
    
    /**
     * 
     * @param string $gameClassName
     */
    public function __construct($gameClassName) {
        $this->gameClassName = $gameClassName;
    }

    /**
     * @see Wrench\Application.Application::onConnect()
     */
    public function onConnect($client) {
        $this->clients[] = $client;
    }

    /**
     * @see Wrench\Application.Application::onUpdate()
     */
    public function onUpdate() {
        // refresh Yii Db Connection
        if (time() > $this->lastTimestamp + 60 * $this->refreshDbInterval) {
            echo "Refreshing DB connection \n";
            $this->lastTimestamp = time();
            Yii::app()->db->setActive(false);
            Yii::app()->db->setActive(true);
        }
    }

    protected function _registerConnection($input, $connection){
        $player = new Lobby\Player($connection, $input->uid);
        if ($player && $player->getModel() && !isset($this->players[$connection->getId()]) && $input->userLobbyToken === $player->getModel()->lobby_reg_token){
            $player->getModel()->generateLobbyToken();
            $this->players[$connection->getId()] = $player;
            $df = new Lobby\DataFrame('registered', $player);
        } else {
            $df = new Lobby\DataFrame('error', 'register_fail');
        }
        $connection->send($df);
    }
    
    protected function _getRoomsListAction($input, $player){
        $rooms = $this->rooms;
        $df = new Lobby\DataFrame('roomsList', $rooms);
        $player->send($df);
    }
    
    protected function _createRoomAction($input, $player) {
        $existedRoom = $this->_getRoomOwnedByPlayer($player);
        if ($existedRoom){
            $df = new Lobby\DataFrame('error', 'player_has_already_created_room');
            $player->send($df);
            return;
        }
        if (!empty($input->name)) {
            $room = new \Lobby\Room($player, $input->name);
            $this->rooms[$room->getId()] = $room;
            $df = new Lobby\DataFrame('createdRoom', $room);
            $player->getConnection()->send($df);
            $room->addMember($player);
            $df = new Lobby\DataFrame('joined', $room);
            $player->getConnection()->send($df);
            $this->_updateClientsRoomsList();
        } else {
            $df = new Lobby\DataFrame('error', 'room_name_is_empty');
            $player->send($df);
        }
    }
    
    protected function _joinRoomAction($input, $player) {
        if (isset($this->rooms[$input->roomId])) {
            $room = $this->rooms[$input->roomId];
            $room->addMember($player);
            $df = new Lobby\DataFrame('joined', $room);
            $this->_updateClientsRoomMembers($room);
            
            $game = $this->_findGameInRoom($room);
            if ($game){
                $game->onLaterJoinRoom($player);
            }
        } else {
            $df = new Lobby\DataFrame('error', 'room_not_found');
        }
        $player->getConnection()->send($df);
        
    }
    
    protected function _leaveRoomAction($input, $player) {
        if (isset($this->rooms[$input->roomId])) {
            $room = $this->rooms[$input->roomId];
            $df = new Lobby\DataFrame('leftRoom', $room);
            if ($room->getOwner()->uid === $player->uid){
                foreach ($room->members as $p){
                    $p->send($df);
                }
                $this->_beforeCloseRoom($room);
                $room = null;
                unset($this->rooms[$input->roomId]);
                $this->_updateClientsRoomsList();
            } else {
                $player->send($df);
                $this->_beforeRemoveRoomMember($room, $player);
                $room->removeMember($player);
                $this->_afterRemoveRoomMember($room, $player);
                $this->_updateClientsRoomMembers($room);
            }
        }
    }
    
    protected function _startGameAction($input, $player) {
        $room = $this->_getRoomOwnedByPlayer($player);
        if ($room){
            /** @var $game \Lobby\GameAbstractct */
            $game = new $this->gameClassName($room);
            $this->games[$game->getId()] = $game;
            
            $gameStd = new stdClass();
            $gameStd->id = $game->getId();
            $gameStd->count = count($this->games);
            
            $df = new Lobby\DataFrame('started', $gameStd);
            foreach ($game->getPlayers() as $player){
                $player->getConnection()->send($df);
            }
        } else {
            $df = new Lobby\DataFrame('error', 'player_does_not_own_room');
            $player->getConnection()->send($df);
        }
    }
    
    protected function _inGamePlayersListAction($input, $player){
        $game = $this->_findGameWithPlayer($player);
        if ($game){
            $df = new Lobby\DataFrame("inGamePlayersList", $game->getPlayers());
            $player->send($df);
        }
    }
    
    protected function _leaveGameAction($input, $player){
        $game = $this->_findGameWithPlayer($player);
        if ($game){
            $game->removePlayer($player);
            $df = new Lobby\DataFrame("leftGame");
            $player->send($df);
            
            $df2 = new Lobby\DataFrame("someOneLeftGame", $player);
            foreach ($game->getPlayers() as $p){
                $p->send($df2);
            }
            $this->_afterLeaveGame($game, $player);
        }
    }
    
    protected function _chatMessageAction($input, $player){
        $room = $this->_findRoomWithPlayer($player);
        if ($room && isset($input->text) && $input->text){
            $data = new stdClass();
            $data->text = CHtml::encode($input->text);
            $data->sender = $player;
            $df = new Lobby\DataFrame('chatMessage', $data);
            foreach ($room->members as $p){
                $p->send($df);
            }
        }
    }
    
    protected function _unregisteredCall($input, $connection){
        $df = new Lobby\DataFrame('unregistered_call');
        $connection->send($df);
    }

    public function onData($payload, $connection) {
        echo "OnData \n";
        try {
            $input = json_decode($payload->buffer);
            if ($input) {
                
                $player = $this->_getPlayerByConnection($connection);
                
                if ($input->action === 'register') {
                    $this->_registerConnection($input, $connection);
                } else if ($player) {
                    $method = '_' . $input->action . 'Action';
                    if (method_exists($this, $method)) {
                        $this->$method($input, $player);
                    } else {
                        $game = $this->_findGameWithPlayer($player);
                        if ($game){
                            $game->onCommand($input, $player);
                        }
                    }
                } else {
                    $this->_unregisteredCall($input, $connection);
                }
                
                
            } else {
                var_dump("Buffer is empty");
            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }
    
    public function onDisconnect($connection){
        $player = $this->_getPlayerByConnection($connection);
        if ($player){
            $player->markAsDisconnected();
            $this->_closeRoomOwnedByConnection($connection);
            $this->_removePlayerFromRooms($player);
        }
       
    }
    
    protected function _updateClientsRoomsList() {
        foreach ($this->players as $p) {
            if (!$this->_isPlayerInRoom($p)){
                $this->_getRoomsListAction(null, $p);
            }
        }
    }
    
    protected function _updateClientsRoomMembers(Lobby\Room $room) {
        $df = new Lobby\DataFrame('updateRoomMembers', $room->members);
        foreach ($room->members as $player) {
            $player->send($df);
        }
    }
    
    protected function _removePlayerFromRooms(\Lobby\Player $player){
        foreach ($this->rooms as $room){
            foreach ($room->members as $mPlayer){
                if ($mPlayer->uid === $player->uid){
                    $this->_beforeRemoveRoomMember($room, $player);
                    $room->removeMember($player);
                    $this->_afterRemoveRoomMember($room, $player);
                    $this->_updateClientsRoomMembers($room);
                }
            }
        }
    }
    
    protected function _beforeRemoveRoomMember(\Lobby\Room $room, \Lobby\Player $player) {
        $game = $this->_findGameWithPlayer($player);
        if ($game){
            $game->removePlayer($player);
        }
    }
    
    protected function _afterRemoveRoomMember(\Lobby\Room $room, \Lobby\Player $player) {
        if (count($room->members) == 0) {
            $this->_beforeCloseRoom($room);
            unset($this->rooms[$room->getId()]);
            $room = null;
            $this->_updateClientsRoomsList();
        }
    }
    
    protected function _beforeCloseRoom(\Lobby\Room $room){
        var_dump("before close", count($this->games));
        $game = $this->_findGameInRoom($room);
        if ($game){
            $this->_beforeStopGame($game);
            unset($this->games[$game->getId()]);
            $game = null;
        }
        $df = new Lobby\DataFrame('leftRoom', $room);
        foreach ($room->members as $player){
            $player->send($df);
        }
    }
    
    protected function _beforeStopGame(\Lobby\GameAbstract $game){
        $df = new Lobby\DataFrame('gameStopped');
        foreach ($game->getPlayers() as $player){
            $player->send($df);
        }
    }
    
    protected function _afterLeaveGame(\Lobby\GameAbstract $game, \Lobby\Player $player) {
        if (count($game->getPlayers()) == 0) {
            unset($this->games[$game->getId()]);
            $game = null;
        }
    }
    
    protected function _closeRoomOwnedByConnection($connection) {
        $player = $this->_getPlayerByConnection($connection);
        if ($player) {
            $room = $this->_getRoomOwnedByPlayer($player);
            if ($room) {
                $this->_beforeCloseRoom($room);
                unset($this->rooms[$room->getId()]);
                $this->_updateClientsRoomsList();
                
                /* somethimes does not close games */
                foreach ($this->games as $game){
                    $room = $game->getRoom();
                    if (!$room || !isset($this->rooms[$room->getId()])){
                        unset($this->games[$game->getId()]);
                        $game = null;
                        var_dump("broken game was closed");
                    }
                }
            }
        }
    }
    
    protected function _getRoomOwnedByPlayer(\Lobby\Player $player){
        foreach ($this->rooms as $room){
            if ($room->getOwner()->uid === $player->uid){
                return $room;
            }
        }
    }
    
    protected function _getPlayerByConnection($connection){
        if (isset($this->players[$connection->getId()])) {
            return $this->players[$connection->getId()];
        }
    }
    
    protected function _isPlayerInRoom(\Lobby\Player $player){
        foreach ($this->rooms as $room){
            foreach ($room->members as $mPlayer){
                if ($mPlayer->uid === $player->uid){
                    return true;
                }
            }
        }
        return false;
    }
    
    protected function _findGameWithPlayer(\Lobby\Player $player){
        foreach ($this->games as $game){
            foreach ($game->getPlayers() as $p){
                if ($p->getId() === $player->getId()){
                    return $game;
                }
            }
        }
    }
    
    protected function _findRoomWithPlayer(\Lobby\Player $player){
        foreach ($this->rooms as $room){
            foreach ($room->members as $mPlayer){
                if ($mPlayer->uid === $player->uid){
                    return $room;
                }
            }
        }
    }
    
    public function _findGameInRoom(\Lobby\Room $room){
        foreach ($this->games as $game){
            if ($game->getRoom()->getId() == $room->getId()){
                return $game;
            }
        }
    }
}
