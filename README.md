Checkers - шашки
==========================

### Описание
Игра в шашки, написанная с использованием Yii Framework,  
на основе [Lobby](https://bitbucket.org/rnix/lobby) для клиент-серверного взаимодействия с использованием  
[Wrench](https://github.com/varspool/Wrench) для WebSocket-сервера,  
[Pixi](https://github.com/GoodBoyDigital/pixi.js) для 2D-рендеринга на стороне клиента,  
а также использующая сервис достижений [http://livelevel.getid.org/](http://livelevel.getid.org/).

### Запуск
* Создать базу данных, используя схему из проекта для MySQL Workbench CE,
который расположен в other/schema.mwb
* В дерикториях public и protected создать файл с именем APPLICATION_ENV.php,
который возвращает значение переменной окружения, например 'dev_home':

        <?php return 'dev_home';

* Настроить путь к Yii framework для соответствующего case в файлах
public/index.php (для веб) и protected/yiic.php (для серверной части web socket)
* Заполнить и переименовать config/example_server.php в server.%APP_ENV%.php, например,
server.dev_home.php 
* Запустить сервер, который будет слушать web socket соединения:

    php protected/yiic.php server
    
### Run in docker
1. `cp docker-compose.dist.yml docker-compose.yml`
2. Optional `cp protected/config/example_server.php protected/config/server.local.php`
3. Set permissions (Linux):
```
sudo find public/assets protected/runtime -type d -exec chmod 777 {} \;
```
4. `docker-compose up -d --build`
5. Run migration
```
docker cp other/checkers.sql $(docker-compose ps -q mysql):/tmp/dump.sql
docker-compose exec mysql sh -c "mysql checkers -ucheckers -pcheckers < /tmp/dump.sql"
docker-compose restart checkers-ws
```

### Краткое описание структуры
Серверная логика игры находится в checkers/app/checkers/CheckersGame.php.  
Клиентская часть - checkers/protected/views/assets/checkers/js/checkers.js.  
Вход через LiveLevel возможен только, если на самом сервисе создано приложение  
в разделе [http://livelevel.getid.org/dev](Разработчикам) и полученные ID и защищенный ключ указаны  
в конфигурации компонента eauth->services->livelevel (см. checkers/protected/config/example_server.php).  
Но ничего страшного, если эти параметры будут пустыми, анонимный вход будет работать.  
Так же не обязательно указывать id для достижений.

### Лицензия MIT
    The MIT License

    Copyright (C) 2013 Roman Nix

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.


