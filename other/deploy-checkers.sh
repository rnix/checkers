#!/bin/bash
#
# deploy project
#
#

TARG="/var/www/checkers.livelevel.net"

TEMPDIR="$HOME/temp_deploy_checkers"
HGSOURCE="ssh://hg@bitbucket.org/rnix/checkers"

EMPTYDIRS=""
HTACCESS="./.htaccess"

rm -rf "$TEMPDIR"
mkdir "$TEMPDIR";
cd "$TEMPDIR" || exit 1

echo hg clone "$HGSOURCE" "$TEMPDIR" 
hg clone "$HGSOURCE" "$TEMPDIR"

cd "$TARG" || exit 1
cd "$TEMPDIR" 

echo hg archive -r tip "$TARG" 
hg archive -r tip "$TARG"

cd "$TARG"

# create empty dirs
for emp in $EMPTYDIRS; do
    mkdir "$emp"
    chmod go+w "$emp"
done

chmod 777 protected/yiic
rm -rfv public/assets/*

echo rm -rf "$TEMPDIR"
rm -rf "$TEMPDIR"

echo OK!

