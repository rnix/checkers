-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 07 2020 г., 05:12
-- Версия сервера: 5.7.16
-- Версия PHP: 5.6.27-1~dotdeb+zts+7.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `checkers`
--

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `uid` varchar(32) NOT NULL,
  `name` varchar(90) DEFAULT NULL,
  `lll_access_token` varchar(32) DEFAULT NULL,
  `lll_token_expired_in` int(11) DEFAULT NULL,
  `lll_user_id` int(10) unsigned DEFAULT NULL,
  `ins_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `defeat_num_pve` int(10) unsigned DEFAULT NULL,
  `defeat_num_pvp` int(10) unsigned DEFAULT NULL,
  `victory_num_pve` int(10) unsigned DEFAULT NULL,
  `victory_num_pvp` int(10) unsigned DEFAULT NULL,
  `lobby_reg_token` varchar(32) DEFAULT NULL,
  `lobby_reg_token_upd_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Структура таблицы `user_ach`
--

CREATE TABLE IF NOT EXISTS `user_ach` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(32) NOT NULL,
  `aid` int(10) unsigned NOT NULL,
  `ins_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_fk` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;


--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `user_ach`
--
ALTER TABLE `user_ach`
  ADD CONSTRAINT `user_fk` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
