FROM php:7.4-fpm

ARG WEB_USER_ID=33
ARG WEB_USER_NAME=www-data
RUN useradd -m -u ${WEB_USER_ID} ${WEB_USER_NAME} || echo "User exists, it's ok." \
    && sed -i -- "s/user = www-data/user = ${WEB_USER_NAME}/g" /usr/local/etc/php-fpm.d/www.conf

RUN apt-get update && apt-get install git unzip locales locales-all -y \
    && docker-php-ext-install pdo pdo_mysql

RUN curl https://codeload.github.com/yiisoft/yii/zip/1.1.13 -o /tmp/yii.zip \
    && unzip /tmp/yii.zip -d /yii && ls /yii

USER ${WEB_USER_ID}
EXPOSE 7880
