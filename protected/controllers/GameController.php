<?php

class GameController extends Controller {

    public function beforeAction($action) {
        if (parent::beforeAction($action)) {
            if (Yii::app()->user->isGuest) {
                Yii::app()->user->setReturnUrl(array('game/index'));
                $this->redirect(Yii::app()->user->loginUrl);
            }
            $js = Yii::app()->getClientScript();
            if (defined('YII_DEBUG') && YII_DEBUG){
                $debug = "true";
            } else {
                $debug = "false";
            }
            $js->registerScript(
                    'lobby-init', "
                        var webSocketAddress = '" . Yii::app()->params['webSocketAddress'] . "';
                        var siteUid = '" . Yii::app()->user->getModel()->uid . "';
                        var siteUserLobbyToken = '" . Yii::app()->user->getModel()->generateLobbyToken() . "';
                        var siteDebugMode = " . $debug . ";
                     ", CClientScript::POS_BEGIN);

            $lobbyPackage = array(
                'basePath' => 'application.views.assets.lobby',
                'css' => array('css/style.css',),
                'js' => array('js/jquery.lobby.js', 'js/connection.js'),
                'depends' => array('jquery')
            );

            Yii::app()->assetManager->publish(Yii::getPathOfAlias($lobbyPackage['basePath']), false, -1, defined('YII_DEBUG') && YII_DEBUG);
            Yii::app()->clientScript->addPackage('lobbyPack', $lobbyPackage)->registerPackage('lobbyPack');


            return true;
        } else {
            return false;
        }
    }

    public function actionIndex(){
        $myPackage = array(
            'basePath' => 'application.views.assets.checkers',
            'css' => array(),
            'js' => array('js/pixi.dev.js', 'js/checkers.js',),
            'img' => array('img/bubble_32x32.png',),
            'depends' => array('jquery')
        );

        $packUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias($myPackage['basePath']), false, -1, defined('YII_DEBUG') && YII_DEBUG);
        Yii::app()->clientScript->addPackage('checkersPack', $myPackage)->registerPackage('checkersPack');
        
        $this->render('index', array(
            'checkersAssetsUrl'=>$packUrl,
            ));
    }
}