<?php
/* edit and rename this file to server.local.php */
return array(
    'params' => array(
        'webSocketAddress' => 'wss://checkers-ws.getid.org',
        'webSocketServerPort' => '7880',
        'webSocketOptions' => array(
            'check_origin' => false,
            'allowed_origins' => array('checkers.getid.org'),
        ),
        'livelevelAchs' => array(
            'one_victory' => 0, //livelevel achievement id
        ),
    ),
    'components' => array(
        'eauth' => array(
            'services' => array(
                'livelevel' => array(
                    'client_id' => '',
                    'client_secret' => '',
                ),
            ),
        ),
    )
);
