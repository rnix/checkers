<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name . ' - Menu';
?>

<?php

?>

<script>
  var checkersAssetsUrl = "<?=$checkersAssetsUrl;?>";
</script>

<div class="row">

    <div class="alert alert-info connection-wait">
        <h3>Происходит подключение</h3>
        <div class="progress progress-striped active">
            <div class="bar" style="width: 100%;"></div>
        </div>
    </div>
    
    <div class="alert alert-danger connection-error" style="display: none;">
        Не удалось подключиться к серверу.
    </div>
    
    <div class="alert alert-error you-lose" style="display: none;">
        Вы проиграли!
    </div>
   
    <div class="alert alert-success you-won" style="display: none;">
        Вы победили!
    </div>
    
    <div class="alert alert-success game-is-over" style="display: none;">
        Игра окончена.
    </div>

    <div class="span8 stage-fixed-width">
        <div class="stage-container"></div>
    </div>
    <div class="span4">
        <div class="rooms-list-container" style="display: none;"> 
            <h4>Список комнат</h4>
            <em class="empty-rooms-list-message" style="display: none;">ни одной комнаты не создано</em>
            <ul class="rooms-list">
                <li class="room-item-tpl" style="display: none;">
                    <span class="room-item-name">name</span>
                </li>
            </ul>
            
            <input type="text" name="roomName"><br><button class="btn createRoom">Создать комнату</button>
        </div>
        
        
        <div class="game-info-container" style="display: none;">
            <div class="side-select" style="display: none;">
                <button class="btn btn-primary play-as-white">Играть белыми</button>
                <button class="btn btn-primary play-as-black">Играть чёрными</button>
            </div>
            
            <div class="selectedSides">
                <div class="i-play-as-white" style="display: none;">Вы играете белыми.</div>
                <div class="i-play-as-black" style="display: none;">Вы играете чёрными.</div>
                <div class="i-play-as-none" style="display: none;">Вы не играете.</div>
                
                <h4 class="your-turn" style="display: none">Ваш ход!</h4>
                <h5 class="not-your-turn" style="display: none">Ход вашего противника</h5>
            </div>
            
            <br><button class="btn leaveGame">Покинуть игру</button>
        </div>
        
        <div class="room-chat-container" style="display: none;">
            <div class="chat-lines">
                <div class="chat-message chat-message-tpl" style="display: none;"><span class="chat-author">author</span>: <span class="chat-text">text</span></div>
            </div>
            <input type="text" class="input-chat-text">
            <button class="btn chat-send">Отправить</button>
        </div>
        
        
        <div class="players-list-container" style="display: none;"> 
            <h4>Список игроков в комнате <span class="room-name"></span></h4>
            <em class="room-is-empty-message" style="display: none;">комната пуста</em>
            <ul class="players-list">
                <li class="player-item-tpl" style="display: none;">
                    <span class="player-item-name">name</span> <span class="player-side player-side-white" style="display: none">(играет белыми)</span><span class="player-side player-side-black" style="display: none">(играет чёрными)</span>
                </li>
            </ul>
            
            <br><button class="btn leaveRoom">Покинуть комнату</button>
            <div class="start-game-container" style="display: none;">
                <br><button class="btn startGame">Запустить игру</button>
            </div>
        </div>
        
        
    </div>
    

</div>