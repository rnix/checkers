(function( $ ){
    
    var ws, settings;
    var DEBUG = false;
    var bindings = {};
    
    var methods = {
        init : function( options ) { 
           
           if (ws){
               return;
           }
           
            settings = $.extend( {
                "wsLobbyAddress": "ws://localhost/lobby",
                "uid": "",
                "userLobbyToken": "",
                "wsOnClose": function() {
                    setTimeout(function(){
                        location.href = location.href;
                    }, 2000);
                },
                "wsOnStart": function(){},
                "requestHandler": function (){},
                "debug": false,
                "useHashNavigation": true
            }, options);
       
            if (settings["debug"]){
                DEBUG = true;
            }
            
            if ("WebSocket" in window){
                try{
                    ws = new WebSocket(settings["wsLobbyAddress"]);
                    ws.onopen = function(e){
                        settings["wsOnStart"](e);
                        start();
                    };
                    ws.onmessage = onMessage;
                    ws.onclose = settings["wsOnClose"];
                } catch (e) {
                    settings["wsOnClose"](e);
                }
            } else {
                alert('Ваш браузер не поддерживает вебсокеты');
            }
        
            var start = function(){
                var data = {
                    "action": "register", 
                    "uid": settings["uid"],
                    "userLobbyToken": settings["userLobbyToken"]
                };
                methods.sendData(data);  
            };
        
            function onMessage (evt){
                var input = $.parseJSON(evt.data);
                if (DEBUG) console.log('input', input);
                var rh = new settings["requestHandler"]();
                var method = 'on' + input.action.charAt(0).toUpperCase() + input.action.slice(1);
                
                if (typeof bindings[method] === 'object'){
                    for (var i = 0; i < bindings[method].length; i++){
                        bindings[method][i](input.data);
                        
                    }
                }
                
                if (typeof rh[method] === 'function'){
                    rh[method](input.data);
                } else if (DEBUG) {
                    //console.warn('method not found: ' + method);
                }
            }
            
            
            if (settings["useHashNavigation"]){
                methods.bind('onRegistered', function(data){
                    var hashData = getHashData();
                    if (typeof hashData["roomId"] != "undefined"){
                        methods.joinRoom(hashData["roomId"]);
                    }
                });
            
                methods.bind('onJoined', function(data){
                    setHashData({roomId: data.id}, true);
                });
            }
        },
        
        
        sendData : function( data ) {
            if (DEBUG) {
                console.log('output', data);
            }
            ws.send(JSON.stringify(data));
        },
        
        joinRoom: function ( roomId ) {
            var data = {
                "action": "joinRoom", 
                "roomId": roomId
            };
            methods.sendData(data);  
        },
        
        createRoom: function ( roomName ){
            var data = {
                "action": "createRoom", 
                "name": roomName
            };
            methods.sendData(data);  
        },
        
        leaveRoom: function (roomId){
            var data = {
                "action": "leaveRoom", 
                "roomId": roomId
            };
            methods.sendData(data);  
        },
        
        getRoomsList: function (){
            var data = {
                "action": "getRoomsList"
            };
            methods.sendData(data);  
        },
        
        startGame: function(){
            var data = {
                "action": "startGame"
            };
            methods.sendData(data);
        },
        
        leaveGame: function(){
            var data = {
                "action": "leaveGame"
            };
            methods.sendData(data);
        },
        
        chatMessage: function(text){
            var data = {
                "action": "chatMessage",
                "text": text
            };
            methods.sendData(data);
        },
        
        bind: function(action, callback){
            if (typeof bindings[action] == "undefined"){
                bindings[action] = [];
            }
            bindings[action].push(callback);
        }
    };
    
    function getHashData(){
        var hrefArr = window.location.href.split('#');
        var hashStr = "";
        var data = {};
        if (hrefArr.length>1){
            hashStr = hrefArr[1];
            var hashArr = hashStr.split('/');
            if (hashArr.length){
                for(var i=0; i<hashArr.length - 1; i=i+2){
                    data[hashArr[i]] = hashArr[i+1];
                }
            }
        }
        return data;
    }
    
    function setHashData(hashData, extend){
        if (extend){
            var currentHashData = getHashData();
            var newHashData = $.extend(currentHashData, hashData);
        } else {
            var newHashData = hashData;
        }
        
        var hashStr = "";
        for(var prop in newHashData){
            hashStr += prop + "/" + newHashData[prop];
        }
        window.location.hash = hashStr;
    }
        
    $.fn.lobby = function( method ) {
        
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.lobby' );
        } 

    };
    
})( jQuery );
