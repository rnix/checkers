
$(function(){
    
    // create an new instance of a pixi stage
    var stage = new PIXI.Stage(0x66FF99, true);
 
    // create a renderer instance.
    var renderer = PIXI.autoDetectRenderer(512, 512);
 
    // add the renderer view element to the DOM
    $(".stage-container").append(renderer.view);
    requestAnimFrame( animate );

    var boardTexture = PIXI.Texture.fromImage(checkersAssetsUrl + "/img/board.jpg");
    var tilingBoard = new PIXI.TilingSprite(boardTexture, 512, 512);
    tilingBoard.tileScale.x = 1;
    tilingBoard.tileScale.y = 1;
    stage.addChild(tilingBoard);
    
    var highlightedTexture = PIXI.Texture.fromImage(checkersAssetsUrl + "/img/highlighted.png");
    var hlObjects = [];
    for (var i=0; i<32; i++){
        var x = (i % 4) * 2;
        var y = Math.floor(i / 4);
        if (y % 2 === 0){
            x = x+1;
        }
        var hl = new PIXI.Sprite(highlightedTexture);
        hl.position.x = x * 64;
        hl.position.y = y * 64;
        hl.visible = false;
        stage.addChild(hl);
        hlObjects.push(hl);
    }
        
    var mySide;
    var positions = {};
    var availableMoves = {};
    var kings = {};
    var units = {"white": [], "black": []};
    var whiteSidePlayerId, blackSidePlayerId;
    
    var requestStateIntervalId;
    
    var whiteTexture = PIXI.Texture.fromImage(checkersAssetsUrl + "/img/white.png");
    var blackTexture = PIXI.Texture.fromImage(checkersAssetsUrl + "/img/black.png");
    var whiteKingTexture = PIXI.Texture.fromImage(checkersAssetsUrl + "/img/white_king.png");
    var blackKingTexture = PIXI.Texture.fromImage(checkersAssetsUrl + "/img/black_king.png");
    
    function unitIdIsKing(side, unitId){
        return (kings && typeof kings[side] != "undefined" && kings[side].indexOf(unitId) > -1);
    }
    
    function allocate(side){
        var texture, kingTexture;
        var id;
        var unit;  
        if (side == "black"){
            texture = blackTexture;
        } else {
            texture = whiteTexture;
        }
        
        //clear
        for (unit in units[side]){
            units[side][unit].visible = false;
        }
        units[side] = [];
        
        for (id in positions[side]){
            unit = new PIXI.Sprite(texture);
            unit.id = id;
            unit.side = side;
            unit.position = convertMiniCoordsToCanvas(positions[side][id]);
            if (mySide == side){
                applyInteractions(unit);
            }
            stage.addChild(unit);
            units[side].push(unit);
        }
    }    
    
    function updatePositions(side){
        var i, unit, kingTexture, kingUnit;
        for (i=0; i<units[side].length; i++){
            unit = units[side][i];
            
            if (unitIdIsKing(side, unit.id) && typeof unit.isKing == "undefined"){
                if (side == "black"){
                    unit.setTexture(blackKingTexture);
                } else {
                    unit.setTexture(whiteKingTexture);
                }
                unit.isKing = true;
            }
            
            if (positions[side][unit.id]){
                unit.position = convertMiniCoordsToCanvas(positions[side][unit.id]);
            } else {
                unit.visible = false;
            }
        }
    }
    
    function animate() {
        requestAnimFrame( animate ); 
        renderer.render(stage);
    }
    
    function hideAvailableMoves(){
        for (var i=0; i<32; i++){
            hlObjects[i].visible=false;
        }
    }
    
    function showAvailableMoves(unit){
        var i, k, pos;
        if (typeof availableMoves[unit.side][unit.id] != "undefined" && availableMoves[unit.side][unit.id]){
            for (k in availableMoves[unit.side][unit.id]){
                pos = availableMoves[unit.side][unit.id][k];
                i = convertMiniCoordsToCellIndex(pos);
                hlObjects[i].visible=true;
            }
        }  
    }
    
    /**
     * (1;2) -> (64, 128)
     */
    function convertMiniCoordsToCanvas(coords){
        var x = coords.x;
        var y = coords.y;
        var pos = {"x": x * 64, "y": y * 64};
        if (mySide == "black"){
            pos.x = 64*7 - pos.x;
            pos.y = 64*7 - pos.y;
        }
        return pos;
    }
    
    /**
     * (68, 129) -> (1, 2)
     */
    function convertCanvasCoordsToMini(coords){
        var x = coords.x;
        var y = coords.y;
        var pos = {"x": Math.floor(x / 64), "y": Math.floor(y / 64)};
        if (mySide == "black"){
            pos.x = 7 - pos.x;
            pos.y = 7 - pos.y;
        }
        return pos;
    }
    
    
    /**
     * (7; 1) -> 7
     */
    function convertMiniCoordsToCellIndex(coords){
        var offsetX = 0;
        if (coords.y % 2 === 0){
            offsetX = 1;
        }
        var index = coords.y * 4 + (coords.x - offsetX) / 2;
        if (mySide == "black"){
            index = 31 - index;
        }
        return index;
    }
                
    function calcDistance(pos1, pos2){
        var d = Math.sqrt(Math.pow(pos2.x - pos1.x, 2) + Math.pow(pos2.y - pos1.y, 2));
        return d;
    }
    
    function dropOnNearest(unit, data, initialPos){
        var i, k, pos;
        var possibilities = [];
        possibilities.push(initialPos);
        if (typeof availableMoves[unit.side][unit.id] != "undefined"){
            for (k in availableMoves[unit.side][unit.id]){
                pos = availableMoves[unit.side][unit.id][k];
                possibilities.push(convertMiniCoordsToCanvas(pos));
            }
        }
        var dropPos = data.global;
        var minDist = 999;
        var nearestPos;
        
        for (i=0; i<possibilities.length; i++){
            var possiblePos = possibilities[i];
            possiblePos.x += 32;
            possiblePos.y += 32;
            var d = calcDistance(dropPos, possiblePos);
            if (d <= minDist){
                minDist = d;
                nearestPos = possiblePos;
            }
        }
             
        unit.position.x = nearestPos.x - 32;
        unit.position.y = nearestPos.y - 32;
        
        if (nearestPos.x !== initialPos.x || nearestPos.y !== initialPos.y){
            onMove(unit, convertCanvasCoordsToMini(nearestPos));
        }
    }
    
    function applyInteractions(sprite){
        sprite.setInteractive(true);
        sprite.buttonMode = true;
        this.dragging = false;
        
        sprite.mousedown = sprite.touchstart = function(data)
        {
            if (this.dragging){
                return;
            }
            // store a refference to the data
            // The reason for this is because of multitouch
            // we want to track the movement of this particular touch
            this.data = data;
            this.dragging = true;
            this.initialPos = $.extend({}, sprite.position);
            showAvailableMoves(sprite);
        };
		
        // set the events for when the mouse is released or a touch is released
        sprite.mouseup = sprite.mouseupoutside = sprite.touchend = sprite.touchendoutside = function(data)
        {
            if (this.dragging == false || typeof this.initialPos == "undefined"){
                return;
            }
            this.dragging = false;
            // set the interaction data to null
            this.data = null;
            hideAvailableMoves();
            dropOnNearest(sprite, data, this.initialPos);
        };
		
        // set the callbacks for when the mouse or a touch moves
        sprite.mousemove = sprite.touchmove = function(data)
        {
            if(this.dragging)
            {
                // need to get parent coords..
                var newPosition = this.data.getLocalPosition(this.parent);
                this.position.x = newPosition.x - 32;
                this.position.y = newPosition.y - 32;
            }
        }
        
    }
    
    function onMove(unit, newPos){
        $().lobby("sendData", {"action" : "move", "data": {"id": unit.id, "newPos": newPos}});
    }
    
    $().lobby("bind", "onStarted", function(){
        $().lobby("sendData", {"action" : "onStart"});
        //for prevent state problem
        requestStateIntervalId = window.setInterval(requestStateFromServer, 30000);
    });
    
    $().lobby("bind", "onSelectSide", function(){
        $(".side-select").show();
    });
    
    $(".play-as-white, .play-as-black").click(function(){
        var side;
        if ($(this).hasClass("play-as-white")){
            side = "white";
        } else {
            side = "black";
        }
        $().lobby("sendData", {"action" : "playAs", "data": side});
    });
    
    $().lobby("bind", "onSelectedSide", function(side){
        $(".i-play-as-" + side).show();
        $(".side-select").hide();
        mySide = side;
    });
    
    
    $().lobby("bind", "onState", function(data){
        if (typeof data.possibleMoves != "undefined"){
            availableMoves = data.possibleMoves;
        }
        if (typeof data.positions != "undefined"){
            positions = data.positions;
        }
        
        if (typeof data.kings != "undefined"){
            kings = data.kings;
        }
                
        switch(data.state){
            case "whiteTurn":
                updatePositions("white");
                updatePositions("black");
                updateTurnHint("white");
                break;
            case "blackTurn":
                updatePositions("white");
                updatePositions("black");
                updateTurnHint("black");
                break;
            case "allocate":
                allocate("white");
                allocate("black");
                whiteSidePlayerId = data.whiteSidePlayerId;
                blackSidePlayerId = data.blackSidePlayerId;
                updatePlayersSidesInfo();
                break;
            case "endMatch":
                if (mySide === data.winningSide){
                    $(".you-won").show();
                } else if (mySide && mySide !== "none") {
                    $(".you-lose").show();
                } else {
                    $(".game-is-over").show();
                }
                setTitle("", false);
                break;
        }
    });
    
    function updatePlayersSidesInfo(){
        $(".players-list li .player-side").hide();
        $(".players-list li").each(function(i, li){
            if ($(li).attr('data-id') == whiteSidePlayerId){
                $(li).find(".player-side-white").show();
            } else if ($(li).attr('data-id') == blackSidePlayerId){
                $(li).find(".player-side-black").show();
            }
        });
    }
    
    function updateTurnHint(turnSide){
        if (turnSide == mySide){
            $(".not-your-turn").hide();
            $(".your-turn").show();
            setTitle("Ваш ход", true);
        } else {
            $(".your-turn").hide();
            $(".not-your-turn").show();
            setTitle("", false);
        }
    }
    
    var baseTitle = document.title;
    var blinkIntervalId;
    function setTitle(title, blink){
        if (blink){
            clearInterval(blinkIntervalId);
            blinkIntervalId = setInterval(function(){
                if (baseTitle == document.title){
                    document.title = title + " | " + baseTitle;
                } else {
                    document.title = baseTitle;
                }
            }, 500);
        } else {
            clearInterval(blinkIntervalId);
            if (title){
                document.title = title + " | " + baseTitle;
            } else {
                document.title = baseTitle;
            }
            
        }
        
    }
    
    $().lobby("bind", "onUpdateRoomMembers", function(data){
        if (!mySide){
            setTitle(Object.keys(data).length + " игрока(ов) в комнате", false);
        }   
    });
    
    $().lobby("bind", "onLeftRoom", function(){
            setTitle("", false);
    });
    
    function requestStateFromServer(){
        $().lobby("sendData", {"action" : "getState"});
    }
    
    $("body").bind("player-list-was-updated", updatePlayersSidesInfo);
    
    $().lobby("bind", "onStarted", function(){
        $(".you-lose, .you-won, .game-is-over, .i-play-as-white, .i-play-as-black, .i-play-as-none, .your-turn, .not-your-turn").hide();
    });
    
    $().lobby("bind", "onSelectedSide", function(){
        $(".game-info-container").show();
    });
});
    