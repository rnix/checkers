<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="hero-unit">
    <h1>Добро пожаловать на <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
    <p>
        Ещё одна игра в шашки. Можно сыграть с определенным игроком без регистрации. Можно запускать со зрителями.
    <h4>Особенности правил:</h4> 
    <ul>
        <li>назад рубить можно и нужно;</li>
        <li>за один ход рубить обязан все из возможных для одной пешки;</li>
        <li>дамка ходит и рубит во всю длину всех своих диагоналей;</li>
    </ul>
    </p>
    <p>
        <a class="btn btn-primary btn-large" href="<?= $this->createUrl('game/index') ?>">
            Играть
        </a>
    </p>
</div>