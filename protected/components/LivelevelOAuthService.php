<?php

/**
 * LivelevelOAuthService class file for Yii EAuth extension
 * @link https://github.com/Nodge/yii-eauth
 *
 * Register application: https://livelevel.getid.org/dev/editApp
 *
 * @author rNix Valentine
 */

/**
 * Livelevel provider class.
 */
class LivelevelOAuthService extends EOAuth2Service {

    protected $name = 'livelevel';
    protected $title = 'LiveLevel';
    protected $client_id = '';
    protected $client_secret = '';
    protected $providerOptions = array(
        'authorize' => 'https://livelevel.getid.org/oauth/authorize',
        'access_token' => 'https://livelevel.getid.org/oauth/access_token',
    );

    protected function fetchAttributes() {
        $info = $this->makeSignedRequest('https://livelevel.getid.org/api/users/info');
        if ($info) {
            $this->attributes['id'] = $info->id;
            $this->attributes['name'] = $info->screen_name;
            $this->attributes['url'] = 'https://livelevel.getid.org/user/' . $info->screen_name;
        }
    }

    protected function getTokenUrl($code) {
        //only url without GET params
        return $this->providerOptions['access_token'];
    }

    protected function getCodeUrl($redirect_uri) {
        //save redirect_uri for getAccessToken()
        $this->setState('redirect_uri', $redirect_uri);
        return parent::getCodeUrl($redirect_uri);
    }

    protected function getAccessToken($code) {
        $params = array(
            'grant_type' => 'authorization_code',
            'code' => $code,
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'redirect_uri' => $this->getState('redirect_uri'), //important!
        );
        return $this->makeRequest($this->getTokenUrl($code), array('data' => $params));
    }

    /**
     * Save access token to the session.
     * @param stdClass $token access token array.
     */
    protected function saveAccessToken($token) {
        $this->setState('auth_token', $token->access_token);
        $this->setState('expires', time() + (isset($token->expires_in) ? $token->expires_in : 365 * 86400) - 60);
        $this->access_token = $token->access_token;
    }

    /**
     * Returns the error info from json.
     * @param stdClass $json the json response.
     * @return array the error array with 2 keys: code and message. Should be null if no errors.
     */
    protected function fetchJsonError($json) {
        if (isset($json->error)) {
            return array(
                'code' => is_string($json->error) ? 0 : $json->error->error_code,
                'message' => is_string($json->error) ? $json->error : $json->error->error_msg,
            );
        } else {
            return null;
        }
    }

    /**
     * Returns the protected resource.
     * @param string $url url to request.
     * @param array $options HTTP request options. Keys: query, data, referer.
     * @param boolean $parseJson Whether to parse response in json format.
     * @return stdClass the response.
     * @see makeRequest
     */
    public function makeSignedRequest($url, $options = array(), $parseJson = true) {
        if (!$this->getIsAuthenticated())
            throw new CHttpException(401, Yii::t('eauth', 'Unable to complete the request because the user was not authenticated.'));
        
        //Use token instead of access_token
        $options['query']['token'] = $this->access_token;
        $result = $this->makeRequest($url, $options);
        return $result;
    }

    /**
     * Returns current access token from state
     * 
     * @return string access token
     */
    public function getStoredAccessToken(){
        return $this->getState('auth_token', null);
    }
    
    /**
     * Returns expire date for current access token
     * 
     * @return int
     */
    public function getStoredAccessTokenExpires(){
        return $this->getState('expires', 0);
    }
}